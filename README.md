# Senior Communities

## Objective

1. Identify high-risk sites specifically related to senior communities:
	* Nursing homes
	* Long-term care facilities
	* Assisted-living facities

2. Calculate localized risk-score for each community** (zip code).

** Assisted living facilities are scraped at the state-level.

**As of 3/24/20 - 29 of 50 states are complete.**

[Tableau Public Link](https://public.tableau.com/profile/kennylee#!/vizhome/FacilityRiskPerZIpCode/RiskScorePerZIPCodeTotalBeds?publish=yes)

[Explanation of the Risk Score](https://docs.google.com/presentation/d/1YdnjuHYuOU7xky16IkQoRjnsP80ARL8RCCZEx\_1S038/edit#slide=id.g71b12dda54\_0\_37)

Contribution by:

* El Ghali Zerhouni: egaz@mit.edu
* Alexandru Socolov: socolov@mit.edu
* Eugenio Zuccarelli: ezucca@mit.edu
* Kenny Lee: kennylee@sloan.mit.edu


# Risk Factors

## Objective

1. Incorporate different risk factors and compute a per-county score showing the risk for that region

* COVID-19 Relative Risk Factors Comparison per State
[Tableau Public Link]https://public.tableau.com/profile/krikor.kirkorov#!/vizhome/RelativeRiskFactorsComparisonperState/Sheet1

The interactive map represents a relative risk profile of counties within each state. The red areas on the map represent regions with higher relative risk compared to the white ones. To categorize counties e produced a score that ranges from 0 to 8, 8 being the highest risk. For each of the risk factors a county can be given 2, 1, or 0 points if it belongs to the top 25%, top 50%, or bottom 50% of counties in the state, respectively. This applies for hypertension, diabetes, and obesity. For age, each county can also get 0, 1, or 2 points if the state has % population over 65 less than 18%, between 18 and 22%, and over 22%.

For example Franklin County in MA has a relative risk score of 3. The score is comprised of:
•	2 points for diabetes due to the county being in the top quartile compared to all other counties in the state
•	0 points for hypertension as this county is in the bottom 50% relative to all other counties in MA
•	1 point for obesity due to the county falling in the second quartile of highest obesity levels in the state
•	0 point for % population over 65 as it has 15.2% of its population over that age.
Data was not available for a small number of geographic areas and for the missing data points we have assumed a value of 0.

Visually we have represented the risk score as the background color of the county on the map where red represents higher relative risk and white represents lower relative risk.

We have overlaid the number of known COVID-19 cases in the form of blue circles on top of the corresponding county. The size of the circles represent the number of cases as the larger the circle, the larger the known cases in the region. We have grouped number of cases in the following buckets: 0, 1-10, 10-100, 100-1000, 1000+. If two counties fall in the same range of COVID-19 cases, then the corresponding blue circles will be the same size.  For example, although Norfolk, MA has more cases than Essex, MA, the two corresponding blue circles are of the same size since they fall in the same group as defined above.

* COVID-19 Relative Risk Factors Comparison Across US
[Explanation of the Risk Score](https://public.tableau.com/profile/krikor.kirkorov#!/vizhome/COVID-19ComplicationRiskFactors/Sheet1?publish=yes)

Similarly to the state-level comparison, we have compared counties across the US. We produced a score for each county that ranges from 0 to 5, 5 being the highest risk. For hypertension, diabetes, and obesity a county can be given 1 or 0 points respectively if it belongs to the top 25% or the bottom 75%. For age, each county can get 0, 1, or 2 points if the state has % population over 65 less than 18%, between 18 and 22%, and over 22%.

For example Charles, Maryland has a relative risk score of 1. The score is comprised of:
•	0 points for diabetes due to the county not being among the top 25% counties nationwide 
•	0 points for hypertension as this county is not among the top 25% counties nationwide
•	1 point for obesity due to the county not being among the top 25% counties nationwide
•	0 point for % population over 65 as it has 9.5% of its population over that age.
Data was not available for a small number of geographic areas and for the missing data points we have assumed a value of 0.

Visually we have represented the risk score as the background color of the county on the map where red represents higher risk and white represents lower risk relative to all counties in US.

* Data and Sources
Population and territorial data extracted from the 2010 decennial census.
Source: https://factfinder.census.gov/

The Diabetes data is for adults aged 18 or older, and represents age-adjusted percentage. The data is from 2016 and is aggregated per county.
Source: https://gis.cdc.gov/grasp/diabetes/DiabetesAtlas.html#

The Hypertension data represents Hospitalization Rate per 1,000 Medicare Beneficiaries, 65+, All Races/Ethnicities, Both Genders, 2014-2016
Source: Hypertension - https://nccd.cdc.gov/DHDSPAtlas/?state=County

The Obesity data represents age-adjusted percentage of people 20 or older. The data is from 2015. 
Source - https://nccd.cdc.gov/DHDSPAtlas/?state=County

The data for COVID-19 cases were pulled from https://github.com/jakobzhao/virus/blob/master/assets/cases.csv
At the time of writing, there was no licensing information available.
The COVID-19 cases data in the location above is not perfect and we have detected several mistakes. To the best of our abilities we have removed the incorrect records and used the rest to produce a summary level data with number of cases per county/state.


* Code
Everything for this section of the project is in https://gitlab.com/covidalliance/riskcommunities/-/tree/master/RiskFactor





Contribution by:
* Krikor Kirkorov: krikor@mit.edu