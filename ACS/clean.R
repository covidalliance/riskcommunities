library(tidyverse)
library(stringr)

raw <- read_csv("raw/ACS_17_5YR_S0101_with_ann.csv")

meta_raw <- read_csv("raw/ACS_17_5YR_S0101_metadata.csv",
                     col_names=c("colname", "verbose"))

meta <- meta_raw %>%
  filter(str_detect(verbose, "AGE"),
         str_detect(verbose, "Total; Estimate;"),
         !str_detect(verbose, "SELECTED AGE")) %>%
  mutate(clean=str_replace(verbose,
                           "Total; Estimate; AGE - ", "") %>%
           str_replace(" to ", "_") %>%
           str_replace(" years", "") %>%
           str_replace(" and over", "_100") %>%
           str_replace("Under ", "0_")) %>%
  separate(clean, into=c("min_age", "max_age")) %>%
  mutate(min_age=as.numeric(min_age), max_age=as.numeric(max_age)) %>%
  mutate(decade=(min_age %/% 10) * 10) %>%
  select(colname, decade)

zip2cty <- (read_csv('../geography/zcta_county_rel_10.txt')
  %>% group_by(ZCTA5, STATE)
  %>% arrange(desc(ZPOP))
  %>% summarise(COUNTY=first(COUNTY)))

clean <- raw %>%
#   select(-GEO.id, -GEO.display.label) %>%
  rename(ZCTA5=GEO.id2) %>%
  gather(colname, pop, -ZCTA5) %>%
  inner_join(meta) %>%
  group_by(ZCTA5, decade) %>%
  summarise(pop=sum(as.numeric(pop))) %>%
  mutate(min_age=decade, max_age=decade + 9) %>%
  select(-decade) %>%
  left_join(zip2cty, by=c("ZCTA5"))

write_csv(clean, "ACS_clean.csv")
