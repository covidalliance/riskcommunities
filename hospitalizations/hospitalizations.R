library(tidyverse)
source('config.R')


aha <- read_csv("raw/ASPUB18.csv", col_types=cols(ID=col_character())) %>%
  select(FSTCD, ID, CNTYNAME, SYSID, MNAME, MLOCZIP, LAT, LONG,
         GENBD, MSICBD, HOSPBD, ADMTOT, MCRNUM, GENHOS)

exclude_re <- "Mental|Rehabilitation|Recovery|Veteran|Behavioral|Specialty|Orthopedic|Orthopaedic"

medicare <- read_csv("raw/2018_HSAF.csv") %>%
  rename(MCRNUM=MEDICARE_PROV_NUM, zip=ZIP_CODE)

hosp <- read_csv("rates_by_age.csv") %>% rename(min_age=age)
zips <- read_csv("../ACS/ACS_clean.csv")

df <- inner_join(zips, hosp, by="min_age") %>%
  filter(pop > 0) %>%
  group_by(STATE, COUNTY, ZCTA5) %>%
  summarise(
    p_hosp=sum(p_hosp * (1 - frac_asymptomatic_cases) * pop) / sum(pop),
    p_icu_given_hosp=sum(p_icu_given_hosp * pop) / sum(pop),
    pop=sum(pop)) %>%
  rename(zip=ZCTA5)

zips_hosp <- (aha
  %>% filter(!is.na(MSICBD) & MSICBD > 0,
             !str_detect(MNAME, exclude_re))
  %>% select(ID, MCRNUM, hospital_state=FSTCD)
  %>% inner_join(medicare, by=c("MCRNUM"))
  %>% inner_join(df, by=c("zip"))
  %>% group_by(zip)
  %>% mutate(pct_cases=TOTAL_CASES / sum(TOTAL_CASES))
  %>% ungroup
  %>% mutate(prorated_pop=pop * pct_cases,
             general_demand=(prorated_pop * (1 - frac_asymptomatic_cases) *
                             p_hosp * (1 - p_icu_given_hosp)),
             icu_demand=(prorated_pop * (1 - frac_asymptomatic_cases) *
                         p_hosp * p_icu_given_hosp)))

write_csv(zips_hosp, "demand_by_zip_and_hospital.csv")
write_csv(df, "hospitalizations.csv")
